# Deploy rke (rancher kubernetes engine)

### Prerequisites
##### on hosts:

* [docker >=18.09.2](https://docs.docker.com/install/)


on all cluster hosts: edit ```/etc/sysctl.conf``` and either uncomment or add the following lines:
```/etc/sysctl.conf
net.ipv4.conf.default.rp_filter=1
net.ipv4.conf.all.rp_filter=1
```

and run the following on each host (so we don't need to restart each host):
```
sysctl net.ipv4.conf.default.rp_filter=1
sysctl net.ipv4.conf.all.rp_filter=1
```

add user to docker group
```
usermod -aG docker [user]
```

##### on dev box:
* [rke](https://github.com/rancher/rke/releases/tag/v0.2.4)
* [helm](https://github.com/helm/helm/releases/tag/v2.14.1)
* [kubectl](https://storage.googleapis.com/kubernetes-release/release/v1.14.3/bin/linux/amd64/kubectl)

we need ssh keys to login to each host, generate on if you don't have one
```
ssh-keygen
```

copy it to each host, for each host run:
```
ssh-copy-id [user]@[host]
```

### rke config

make a directory for our files
```shell
mkdir k8s-cluster
cd k8s-cluster
```

create our config file
```shell
rke config
```

example output (host1 in the example is your raspberry pi):
```shell
> rke config
[+] Cluster Level SSH Private Key Path [~/.ssh/id_rsa]: 
[+] Number of Hosts [1]: 2
[+] SSH Address of host (1) [none]: host1
[+] SSH Port of host (1) [22]: 
[+] SSH Private Key Path of host (host1) [none]: ~/.ssh/id_rsa
[+] SSH User of host (host1) [ubuntu]: user
[+] Is host (host1) a Control Plane host (y/n)? [y]: y
[+] Is host (host1) a Worker host (y/n)? [n]: n
[+] Is host (host1) an etcd host (y/n)? [n]: y
[+] Override Hostname of host (host1) [none]: 
[+] Internal IP of host (host1) [none]: 10.0.0.1
[+] Docker socket path on host (host1) [/var/run/docker.sock]: 
[+] SSH Address of host (2) [none]: host2
[+] SSH Port of host (2) [22]: 
[+] SSH Private Key Path of host (host2) [none]: ~/.ssh/id_rsa
[+] SSH User of host (host2) [ubuntu]: user
[+] Is host (host2) a Control Plane host (y/n)? [y]: n
[+] Is host (host2) a Worker host (y/n)? [n]: y
[+] Is host (host2) an etcd host (y/n)? [n]: n
[+] Override Hostname of host (host2) [none]: 
[+] Internal IP of host (host2) [none]: 10.0.0.2
[+] Docker socket path on host (host2) [/var/run/docker.sock]: 
[+] Network Plugin Type (flannel, calico, weave, canal) [canal]: 
[+] Authentication Strategy [x509]: 
[+] Authorization Mode (rbac, none) [rbac]: 
[+] Kubernetes Docker image [rancher/hyperkube:v1.13.5-rancher1]: 
[+] Cluster domain [cluster.local]: 
[+] Service Cluster IP Range [10.43.0.0/16]: 
[+] Enable PodSecurityPolicy [n]: 
[+] Cluster Network CIDR [10.42.0.0/16]: 
[+] Cluster DNS Service IP [10.43.0.10]: 
[+] Add addon manifest URLs or YAML files [no]:
```

bring up the cluster:
```shell
rke up
```

after that finishes we will have a kubernetes config file in the local directory: ```kube_config_cluster.yml```
you can check the status of the cluster with ```kubectl```
```shell
kubectl --kubeconfig ./kube_config_cluster.yml get pods,svc,ing --all-namespaces
```
to run kubectl without having to specify ```--kubeconfig```
```shell
mkdir ~/.kube
cp kube_config_cluster.yml ~/.kube/config
```

### helm

```shell
# Kubernetes cluster RBAC for helm
kubectl create clusterrolebinding add-on-cluster-admin --clusterrole=cluster-admin --serviceaccount=kube-system:default

# Install helm
helm init
```

### cert-manager

```shell
# Create a namespace to run cert-manager in
kubectl create namespace cert-manager

# Disable resource validation on the cert-manager namespace
kubectl label namespace cert-manager certmanager.k8s.io/disable-validation=true

# Install the CustomResourceDefinitions and cert-manager itself
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v0.8.0/cert-manager.yaml

# Add the Jetstack Helm repository
helm repo add jetstack https://charts.jetstack.io

# Update your local Helm chart repository cache
helm repo update

# Install the cert-manager Helm chart
helm install \
  --name cert-manager \
  --namespace cert-manager \
  --version v0.8.0 \
  jetstack/cert-manager
```

### rancher server

**make sure you change the hostname in the last command**
you can use letsencrypt certs instead of self-signed certs
```shell
# Add rancher Helm chart repo
helm repo add rancher-latest https://releases.rancher.com/server-charts/latest

# Update repo cache
helm repo update

# Install rancher server with Helm
helm install rancher-latest/rancher \
  --name rancher \
  --namespace cattle-system \
  --set ingress.tls.source=rancher \
  --set hostname=rancher.my.org
```

#### access you rancher server

*if you don't have a dns server you'll need to edit your hosts file and add your rancher hostname to it*

browse to  ```https://[rancher hostname]``` and setup you account

## Read entire article before starting [link](https://medium.com/@brotandgames/smallest-kubernetes-cluster-installation-with-rancher-as-cluster-manager-71354d52d78 "link")
